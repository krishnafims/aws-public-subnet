################
# Variable Public subnet
################


# Input variable of Pomelo Public subnet

variable "create_vpc" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  type        = bool
  default     = true
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
  default     = []
}

variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`."
  type        = bool
  default     = false
}

variable "azs" {
  description = "A list of availability zones names or ids in the region"
  type        = list(string)
  default     = []
}

variable "map_public_ip_on_launch" {
  description = "Should be false if you do not want to auto-assign public IP on launch"
  type        = bool
  default     = true
}


variable "public_subnet_name" {
  description = "tagging for the subnet"
  type        = string
  default     = ""
}

variable "subnet_creator"{
 description = "creator user name of resource"
 type        = string
 default    = "Pbarbhui"
}

variable "subnet_region"{
 description = "region for subnet"
 type        = string
 default   = "us-east-1"
}
variable "subnet_env"{
 description = "environment for resource"
 type        = string
 default     = "dev"
}
variable "subnet_atomation"{
 type        = string
 default     = "terraform"
}
variable "subnet_avail"{
 description = "availability zone for subnet"
 type        = string
 default     = "us-east-1"

}
variable "account_id"{
 description = "account id for resource"
 type        = string
 default     = "007"
}
